package scwclient

import (
	"context"
	"fmt"
	"time"

	iam "github.com/scaleway/scaleway-sdk-go/api/iam/v1alpha1"
	"github.com/scaleway/scaleway-sdk-go/api/instance/v1"
	"github.com/scaleway/scaleway-sdk-go/scw"
)

type Client interface {
	ListInstances(ctx context.Context, tag string, commercialType string) (*instance.ListServersResponse, error)
	GetInstance(ctx context.Context, id string) (*instance.GetServerResponse, error)
	DeleteInstances(instanceIDs []*string) error
	CreateInstances(ctx context.Context, delta int, instanceType *string, tag *string) error
	SendSSHPublicKey(ctx context.Context, params *iam.CreateSSHKeyRequest) (*iam.SSHKey, error)
	StartInstance(ctx context.Context, id string) error
}

var _ Client = (*client)(nil)

type client struct {
	instance *instance.API
	iam      *iam.API
}

func New(profile scw.Profile) Client {
	// Create a Scaleway client
	scwClient, err := scw.NewClient(
		scw.WithDefaultOrganizationID(*profile.DefaultOrganizationID),
		scw.WithAuth(*profile.AccessKey, *profile.SecretKey),
		scw.WithDefaultRegion(scw.Region(*profile.DefaultRegion)),
		scw.WithDefaultZone(scw.Zone(*profile.DefaultZone)),
	)

	if err != nil {
		panic(err)
	}

	return &client{
		instance: instance.NewAPI(scwClient),
		iam:      iam.NewAPI(scwClient),
	}
}

func (c *client) ListInstances(ctx context.Context, tag string, commercialType string) (*instance.ListServersResponse, error) {
	return c.instance.ListServers(
		&instance.ListServersRequest{
			Tags:           []string{tag},
			CommercialType: scw.StringPtr(commercialType),
		},
		scw.WithContext(ctx),
	)
}

func (c *client) GetInstance(ctx context.Context, id string) (*instance.GetServerResponse, error) {
	return c.instance.GetServer(
		&instance.GetServerRequest{
			ServerID: id,
		},
		scw.WithContext(ctx),
	)
}

func (c *client) DeleteInstances(instanceIds []*string) error {
	for id := range instanceIds {
		_, err := c.instance.ServerAction(&instance.ServerActionRequest{
			ServerID: *instanceIds[id],
			Action:   instance.ServerActionTerminate,
		})

		return err
	}

	return nil
}

func (c *client) CreateInstances(ctx context.Context, delta int, instanceType *string, tag *string) error {
	for i := 0; i < delta; i++ {
		createImageRes, err := c.instance.ListImages(
			&instance.ListImagesRequest{
				Name:   scw.StringPtr("docker"),
				Arch:   scw.StringPtr(instance.ArchX86_64.String()),
				Public: scw.BoolPtr(true),
			},
			scw.WithContext(ctx),
		)

		if err != nil {
			return err
		}

		name := fmt.Sprintf("%s-%d", *tag, i)

		createServerRequest := &instance.CreateServerRequest{
			Name:              name,
			CommercialType:    *instanceType,
			Tags:              []string{*tag},
			Image:             createImageRes.Images[0].ID,
			DynamicIPRequired: scw.BoolPtr(true),
		}

		createRes, err := c.instance.CreateServer(
			createServerRequest,
			scw.WithContext(ctx),
		)

		if err != nil {
			return err
		}

		// Start the server and wait until it's ready.
		err = c.StartInstance(ctx, createRes.Server.ID)

		if err != nil {
			panic(err)
		}
	}

	return nil
}

func (c *client) SendSSHPublicKey(ctx context.Context, params *iam.CreateSSHKeyRequest) (*iam.SSHKey, error) {
	return c.iam.CreateSSHKey(params, scw.WithContext(ctx))
}

func (c *client) StartInstance(ctx context.Context, id string) error {
	timeout := 5 * time.Minute
	err := c.instance.ServerActionAndWait(&instance.ServerActionAndWaitRequest{
		ServerID: id,
		Action:   instance.ServerActionPoweron,
		Timeout:  &timeout,
	})

	if err != nil {
		return err
	}

	return nil
}
