package fake

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"sync"
	"sync/atomic"

	iamtypes "github.com/scaleway/scaleway-sdk-go/api/iam/v1alpha1"
	instancetypes "github.com/scaleway/scaleway-sdk-go/api/instance/v1"
	"github.com/scaleway/scaleway-sdk-go/scw"
)

type Instance struct {
	InstanceId string
	State      instancetypes.ServerState
	PublicIPs  []*instancetypes.ServerIP
}

type Client struct {
	profile scw.Profile

	Instances []Instance
	DesiredCapacity int

	count atomic.Uint64
}

var once sync.Once
var winrmKey *rsa.PrivateKey

func Key() *rsa.PrivateKey {
	once.Do(func() {
		var err error
		winrmKey, err = rsa.GenerateKey(rand.Reader, 4096)
		if err != nil {
			panic(err)
		}
	})

	return winrmKey
}

func New(profile scw.Profile) *Client {
	return &Client{profile: profile}
}

func (c *Client) CreateInstances(ctx context.Context, delta int, instanceType *string, tag *string) error {
	c.DesiredCapacity += delta
	for len(c.Instances) < delta {
		c.Instances = append(c.Instances, Instance{
			InstanceId: fmt.Sprintf("instance__%d", c.count.Add(1)),
			State:      instancetypes.ServerStateRunning,
			PublicIPs: []*instancetypes.ServerIP{{ State: instancetypes.ServerIPStateAttached }},
		})
	}

	return nil
}

func (c *Client) DeleteInstances(instanceIds []*string) error {
	for index, instance := range c.Instances {
		for _, deleteInstanceId := range instanceIds {
			if *deleteInstanceId == instance.InstanceId {
				c.Instances = append(c.Instances[:index], c.Instances[index+1:]...)
			}
		}
	}

	return nil
}

func (c *Client) ListInstances(ctx context.Context, tag string, commercialType string) (*instancetypes.ListServersResponse, error) {
	instances := make([]*instancetypes.Server, 0, len(c.Instances))
	for _, instance := range c.Instances {
		instances = append(instances, &instancetypes.Server{
			ID:        instance.InstanceId,
			State:     instance.State,
			PublicIPs: instance.PublicIPs,
		})
	}

	return &instancetypes.ListServersResponse{
		Servers: instances,
	}, nil
}

func (c *Client) GetInstance(ctx context.Context, id string) (*instancetypes.GetServerResponse, error) {
	var server *instancetypes.GetServerResponse
	var err error

	for _, instance := range c.Instances {
		if id == instance.InstanceId {
			server = &instancetypes.GetServerResponse{
				Server: &instancetypes.Server{
					ID:        instance.InstanceId,
					State:     instance.State,
					PublicIPs: instance.PublicIPs,
				},
			}
		}
	}

	if server == nil {
		err = fmt.Errorf("not found")
	}

	return server, err
}

func (c *Client) SendSSHPublicKey(ctx context.Context, params *iamtypes.CreateSSHKeyRequest) (*iamtypes.SSHKey, error) {
	return &iamtypes.SSHKey{
		Name:      params.Name,
		ProjectID: params.ProjectID,
		PublicKey: params.PublicKey,
	}, nil
}

func (c *Client) StartInstance(ctx context.Context, id string) error {
	return nil
}
