package scwinstance

import (
	"context"
	"fmt"
	"path"
	"strings"

	"github.com/hashicorp/go-hclog"
	instancetype "github.com/scaleway/scaleway-sdk-go/api/instance/v1"
	"github.com/scaleway/scaleway-sdk-go/scw"
	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-scw/internal/scwclient"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

var _ provider.InstanceGroup = (*InstanceGroup)(nil)

var newClient = scwclient.New

type InstanceGroup struct {
	// AccessKey      string `json:"access_key"`
	ConfigFile string `json:"config_file"`
	// OrganizationID string `json:"organization_id"`
	// ProjectID      string `json:"project_id"`
	// SecretKey      string `json:"secret_key"`
	// Region         string `json:"region"`
	// Zone           string `json:"zone"`
	Type string `json:"type"`
	Name string `json:"name"`

	client scwclient.Client

	log hclog.Logger

	settings provider.Settings

	size int
}

// Init implements provider.InstanceGroup
func (g *InstanceGroup) Init(ctx context.Context, logger hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	var err error
	var cfg *scw.Config
	var profile *scw.Profile
	// var profile = scw.Profile{
	// 	AccessKey:             scw.StringPtr(g.AccessKey),
	// 	SecretKey:             scw.StringPtr(g.SecretKey),
	// 	DefaultOrganizationID: scw.StringPtr(g.OrganizationID),
	// 	DefaultProjectID:      scw.StringPtr(g.ProjectID),
	// 	DefaultRegion:         scw.StringPtr(g.Region),
	// 	DefaultZone:           scw.StringPtr(g.Zone),
	// }

	if g.ConfigFile != "" {
		cfg, err = scw.LoadConfigFromPath(g.ConfigFile)
		if err != nil {
			return provider.ProviderInfo{}, fmt.Errorf("creating scaleway config: %w", err)
		}

		profile = scw.MergeProfiles(&cfg.Profile, scw.LoadEnvProfile())
	} else {
		profile = scw.LoadEnvProfile()
	}

	g.client = newClient(*profile)
	if err != nil {
		return provider.ProviderInfo{}, fmt.Errorf("creating client: %w", err)
	}

	g.settings = settings

	instances, err := g.client.ListInstances(ctx, g.Name, g.Type)
	if err != nil {
		return provider.ProviderInfo{}, fmt.Errorf("fetching instance: %w", err)
	}

	g.size = int(len(instances.Servers))

	return provider.ProviderInfo{
		ID:        path.Join("scw", *profile.DefaultRegion, g.Name),
		MaxSize:   1000,
		Version:   Version.String(),
		BuildInfo: Version.BuildInfo(),
	}, nil
}

// ConnectInfo implements provider.InstanceGroup
func (g *InstanceGroup) ConnectInfo(ctx context.Context, id string) (provider.ConnectInfo, error) {
	info := provider.ConnectInfo{ConnectorConfig: g.settings.ConnectorConfig}

	instances, err := g.client.ListInstances(ctx, g.Name, g.Type)
	if err != nil {
		return info, fmt.Errorf("fetching instance: %w", err)
	}

	if len(instances.Servers) == 0 {
		return info, fmt.Errorf("instance not found")
	}

	var instanceId = instances.Servers[0].ID

	instanceRes, err := g.client.GetInstance(ctx, instanceId)

	if err != nil {
		return info, fmt.Errorf("fetching instance: %w", err)
	}

	instance := instanceRes.Server

	if info.OS == "" {
		if instance.Image != nil && instance.Image.Name != "" {
			info.OS = strings.ToLower(instance.Image.Name)
		} else {
			info.OS = "linux"
		}
	}

	if info.Arch == "" {
		if instance.Arch != "" {
			info.Arch = instance.Arch.String()
		} else {
			info.Arch = "amd64"
		}
	}

	if info.Username == "" {
		info.Username = "root"
	}

	// info.InternalAddr = *instance.PrivateIP
	info.ExternalAddr = instance.PublicIPs[0].Address.String()

	if info.Protocol == "" {
		info.Protocol = provider.ProtocolSSH
		if info.OS == "windows" {
			info.Protocol = provider.ProtocolWinRM
		}
	}

	if info.UseStaticCredentials {
		return info, nil
	}

	switch info.Protocol {
	case provider.ProtocolSSH:
		err = g.ssh(ctx, &info, instance)

	case provider.ProtocolWinRM:
		err = fmt.Errorf("windows not supported")
	}

	return info, err
}

// Decrease implements provider.InstanceGroup
func (g *InstanceGroup) Decrease(ctx context.Context, instances []string) ([]string, error) {
	if len(instances) == 0 {
		return nil, nil
	}

	instanceIds := []*string{}
	for idx := range instances {
		instanceIds = append(instanceIds, &instances[idx])
	}

	err := g.client.DeleteInstances(instanceIds)

	if err != nil {
		return nil, err
	}

	g.size -= len(instances)

	return instances, nil
}

func (g *InstanceGroup) Increase(ctx context.Context, delta int) (int, error) {
	list, err := g.client.ListInstances(ctx, g.Name, g.Type)
	if err != nil {
		return 0, err
	}

	serverDesire := g.size + delta

	for _, server := range list.Servers {

		instanceRes, err := g.client.GetInstance(ctx, server.ID)

		if err != nil {
			return 0, err
		}

		instance := instanceRes.Server

		if instance.State == "stopped" {
			err = g.client.StartInstance(ctx, server.ID)
			if err != nil {
				return 0, err
			}

			delta -= 1
		}
	}

	if delta > 0 {
		err = g.client.CreateInstances(ctx, delta, &g.Type, &g.Name)
		if err != nil {
			return 0, err
		}
	}

	g.size += delta

	return serverDesire, nil
}

func (g *InstanceGroup) Shutdown(ctx context.Context) error {
	return nil
}

func (g *InstanceGroup) Update(ctx context.Context, update func(id string, state provider.State)) error {
	list, err := g.client.ListInstances(ctx, g.Name, g.Type)
	if err != nil {
		return err
	}

	for _, server := range list.Servers {

		instanceRes, err := g.client.GetInstance(ctx, server.ID)

		if err != nil {
			return err
		}

		instance := instanceRes.Server

		state := provider.StateCreating

		switch instance.State {
		case "running":
			for _, publicIp := range instance.PublicIPs {
				if publicIp.State == instancetype.ServerIPStateAttached {
					state = provider.StateRunning
					break
				}
			}
		case "stopped in place", "stopped", "locked":
			state = provider.StateDeleted
		}

		update(instance.ID, state)
	}

	return nil
}
