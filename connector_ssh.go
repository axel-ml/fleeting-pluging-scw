package scwinstance

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"time"

	iam "github.com/scaleway/scaleway-sdk-go/api/iam/v1alpha1"
	"github.com/scaleway/scaleway-sdk-go/api/instance/v1"
	"golang.org/x/crypto/ssh"

	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

type PrivPub interface {
	crypto.PrivateKey
	Public() crypto.PublicKey
}

func (g *InstanceGroup) ssh(ctx context.Context, info *provider.ConnectInfo, instance *instance.Server) error {
	var key PrivPub
	var err error

	if info.Key != nil {
		_, err := ssh.ParseRawPrivateKey(info.Key)
		if err != nil {
			return fmt.Errorf("reading private key: %w", err)
		}
		// var ok bool
		// key, ok = priv.(PrivPub)
		// if !ok {
		// 	return fmt.Errorf("key doesn't export PublicKey()")
		// }
	} else {
		key, err = rsa.GenerateKey(rand.Reader, 4096)
		if err != nil {
			return fmt.Errorf("generating private key: %w", err)
		}

		info.Key = pem.EncodeToMemory(
			&pem.Block{
				Type:  "RSA PRIVATE KEY",
				Bytes: x509.MarshalPKCS1PrivateKey(key.(*rsa.PrivateKey)),
			},
		)

		// @todo: ajouter vérification que clé passée dans settings existe dans scaleway
		sshPubKey, err := ssh.NewPublicKey(key.Public())
		if err != nil {
			return fmt.Errorf("generating ssh public key: %w", err)
		}

		_, err = g.client.SendSSHPublicKey(ctx, &iam.CreateSSHKeyRequest{
			Name:      "fleeting",
			PublicKey: string(ssh.MarshalAuthorizedKey(sshPubKey)),
			ProjectID: instance.Project,
		})

		if err != nil {
			return fmt.Errorf("sending ssh key: %w", err)
		}
	}

	expires := time.Now().Add(60 * time.Second)
	info.Expires = &expires
	// info.Timeout = 60 * time.Second

	return nil
}
