package scwinstance

import (
	"context"
	"crypto/x509"
	"encoding/pem"
	// "bytes"
	"os"
	"testing"

	"github.com/scaleway/scaleway-sdk-go/scw"
	"github.com/hashicorp/go-hclog"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-scw/internal/scwclient"
	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-scw/internal/scwclient/fake"
	instancetypes "github.com/scaleway/scaleway-sdk-go/api/instance/v1"
)

func setupFakeClient(t *testing.T, setup func(client *fake.Client)) *InstanceGroup {
	t.Helper()

	oldClient := newClient
	t.Cleanup(func() {
		newClient = oldClient
	})

	if region, ok := os.LookupEnv("SCW_DEFAULT_REGION"); ok {
		t.Cleanup(func() {
			os.Setenv("SCW_DEFAULT_REGION", region)
		})
	} else {
		t.Cleanup(func() {
			os.Unsetenv("SCW_DEFAULT_REGION")
		})
	}
	os.Setenv("SCW_DEFAULT_REGION", "fake")

	newClient = func(profile scw.Profile) scwclient.Client {
		client := fake.New(profile)
		if setup != nil {
			setup(client)
		}

		return client
	}

	return &InstanceGroup{
		Name: "test-group",
	}
}

func TestIncrease(t *testing.T) {
	group := setupFakeClient(t, nil)

	ctx := context.Background()

	var count int
	_, err := group.Init(ctx, hclog.NewNullLogger(), provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		count++
	}))
	require.Equal(t, 0, group.size)
	require.Equal(t, 0, count)

	num, err := group.Increase(ctx, 2)
	require.Equal(t, 2, num)
	require.NoError(t, err)
	count = 0
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		require.Equal(t, provider.StateRunning, state)
		count++
	}))
	require.Equal(t, 2, group.size)
	require.Equal(t, 2, count)
}

func TestDecrease(t *testing.T) {
	group := setupFakeClient(t, func(client *fake.Client) {
		client.DesiredCapacity = 2
		client.Instances = append(
			client.Instances,
			fake.Instance{
				InstanceId: "pre-existing-1",
				State:      instancetypes.ServerStateRunning,
				PublicIPs: []*instancetypes.ServerIP{{ State: instancetypes.ServerIPStateAttached }},
			},
			fake.Instance{
				InstanceId: "pre-existing-2",
				State:      instancetypes.ServerStateRunning,
				PublicIPs: []*instancetypes.ServerIP{{ State: instancetypes.ServerIPStateAttached }},
			},
		)
	})

	ctx := context.Background()

	var count int
	_, err := group.Init(ctx, hclog.NewNullLogger(), provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		require.Equal(t, provider.StateRunning, state)
		count++
	}))
	require.Equal(t, 2, group.size)
	require.Equal(t, 2, count)

	removed, err := group.Decrease(ctx, []string{"pre-existing-1"})
	require.Contains(t, removed, "pre-existing-1")
	require.NoError(t, err)
	count = 0
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		count++
	}))
	// require.Equal(t, 1, group.client.(*fake.Client).DesiredCapacity)
	require.Equal(t, 1, group.size)
}

func TestConnectInfo(t *testing.T) {
	group := setupFakeClient(t, func(client *fake.Client) {
		client.DesiredCapacity = 1
		client.Instances = append(client.Instances, fake.Instance{
			InstanceId: "pre-existing-1",
			State:      instancetypes.ServerStateRunning,
			PublicIPs: []*instancetypes.ServerIP{{ State: instancetypes.ServerIPStateAttached }},
		})
	})

	ctx := context.Background()

	_, err := group.Init(ctx, hclog.NewNullLogger(), provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {}))

	encodedKey := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(fake.Key()),
		},
	)

	tests := []struct {
		config provider.ConnectorConfig
		assert func(t *testing.T, info provider.ConnectInfo, err error)
	}{
		{
			config: provider.ConnectorConfig{
				OS: "linux",
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.NoError(t, err)
				require.Equal(t, info.Protocol, provider.ProtocolSSH)
				require.NotEmpty(t, info.Key)
			},
		},
		{
			config: provider.ConnectorConfig{
				OS:                   "linux",
				Username:             "username",
				Password:             "password",
				UseStaticCredentials: true,
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.NoError(t, err)
				require.Equal(t, info.Username, "username")
				require.Equal(t, info.Password, "password")
				require.Equal(t, info.Protocol, provider.ProtocolSSH)
				require.Empty(t, info.Key)
			},
		},
		{
			config: provider.ConnectorConfig{
				Protocol: provider.ProtocolSSH,
				Key:      []byte("invalid-key"),
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.ErrorContains(t, err, "reading private key: ssh: no key found")
			},
		},
		{
			config: provider.ConnectorConfig{
				Protocol: provider.ProtocolSSH,
				Key:      encodedKey,
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.NoError(t, err)
				require.Equal(t, info.Protocol, provider.ProtocolSSH)
				require.NotEmpty(t, info.Key)
			},
		},
		{
			config: provider.ConnectorConfig{
				Protocol: provider.ProtocolWinRM,
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.ErrorContains(t, err, "windows not supported")
			},
		},
	}

	for _, tc := range tests {
		t.Run("", func(t *testing.T) {
			group.settings.ConnectorConfig = tc.config

			info, err := group.ConnectInfo(ctx, "pre-existing-1")
			tc.assert(t, info, err)
		})
	}
}
