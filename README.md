# Fleeting Plugin Scaleway

This is a [fleeting](https://gitlab.com/gitlab-org/fleeting/fleeting) plugin for Scaleway.

[![Pipeline Status](https://gitlab.com/axel-ml/fleeting-pluging-scw/badges/main/pipeline.svg)](https://gitlab.com/axel-ml/fleeting-pluging-scw/commits/main)

This project is inspired by :

- https://gitlab.com/gitlab-org/fleeting/plugins/aws
- https://gitlab.com/gitlab-org/fleeting/plugins/googlecloud
- https://gitlab.com/gitlab-org/fleeting/plugins/azure

## Building the plugin

To generate the binary, ensure `$GOPATH/bin` is on your PATH, then use `go install`:

```shell
cd cmd/fleeting-plugin-scw/
go install 
```

If you are managing go versions with asdf, run this after generating the binary:

```shell
asdf reshim
```

## Plugin Configuration

The following parameters are supported:

| Parameter             | Type   | Description |
|-----------------------|--------|-------------|
| `config_file`    | string | Optional. Path to the Scaleway config file ([Scaleway Configuration file settings](https://www.scaleway.com/en/docs/developer-tools/scaleway-cli/reference-content/scaleway-configuration-file/)). |

## WinRM

Windows not supported on Scaleway

## Examples

### GitLab Runner

GitLab Runner has examples on using this plugin for the [Instance executor](https://docs.gitlab.com/runner/executors/instance.html#examples) and [Docker Autoscaler executor](https://docs.gitlab.com/runner/executors/docker_autoscaler.html#examples).
