package main

import (
	scw "gitlab.com/gitlab-org/fleeting/fleeting-plugin-scw"
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
)

func main() {
	plugin.Serve(&scw.InstanceGroup{})
}
